This is a copy of some datasets from
[Financial Administration of Slovak Republic](https://www.financnasprava.sk/sk/elektronicke-sluzby/verejne-sluzby/zoznamy/exporty-z-online-informacnych).

How the copy is made: [with automatic harvester](https://gitlab.com/opendatask/harvester-fssr-vat-subjects)

- TL;DR: download, unpack, push into git

License for data: [CC-BY-NC-ND 4.0](https://creativecommons.org/licenses/by-nc-nd/4.0/)

Downloade data is signed with the key in [GPG-KEY](GPG-KEY). That is not a
full and proper paper trail, but better than nothing.

More info: TODO
